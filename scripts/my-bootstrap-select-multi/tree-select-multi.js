/*
Author: tuanv2t@gmail.com
*/
$.fn.treeSelectMulti = function(options) {
    var self = $(this);
	 self.options = options;
	 self.filteredItems = [];
	 self.idTextMap = {};
	 self.parentIdTextMap = {};
	var controlId = self.attr('id');
	
	//var ul = $('#' + controlId + ' .option-container-list-ul').first();
	
	self.selectedItems = {};
	/*================ Generate HTML ==================== */
	/*All functions used inside generating html should be declared first */		
	self.itemSelectedChange= function()
	{
		//call callback function 
		if(options.itemSelectedChange!=undefined){
			var selectedId=[];
			self.selectedItems = {};//clear selected items
			$('#' + controlId + ' .option-item-checkbox-child').each(function( key, value ) {
			  var chk = $(value);
			  
			  if(chk.is(":checked"))
			  {
				var returnedId = chk.attr('returnedId');
				selectedId.push({Id:returnedId,Text:self.idTextMap[returnedId]});
				if(self.selectedItems[returnedId]==null){
					//rememeber selected item 
					self.selectedItems[returnedId] = self.idTextMap[returnedId];
				}
			  }
			  
			});

			var selectedParentItems = [];

			$('#' + controlId + ' .option-item-checkbox').each(function( key, value ) {
			  var chk = $(value);
			  
			  if(chk.is(":checked"))
			  {
				var optionid = chk.attr('optionid');
				selectedParentItems.push({Id:optionid,Text:self.parentIdTextMap[optionid]});
				
			  }
			  
			});

			options.itemSelectedChange(selectedId,selectedParentItems);
		}
	}
	
	self.showTree = function()
	{
		$( '#' + controlId + ' .option-container' ).slideDown( "slow", function() {
			// Animation complete.
		});
		var indexHighest = 0;
        $("*").each(function () {
            var indexCurrent = parseInt($(this).css("zIndex"), 10);
            if (indexCurrent > indexHighest) {
                indexHighest = indexCurrent;
            }
        });
        $('#' + controlId).css('zIndex', indexHighest + 1);
	}
	self.hideTree = function()
	{
		 $( '#' + controlId + ' .option-container' ).slideUp( "slow", function() {
			// Animation complete.
		});
		$('#' + controlId).find('.select-tree-open').hide();
		$('#' + controlId).find('.select-tree-close').show();
	}
	self.setSelected = function(){

		if(self.options.ableToInitSelectedItems != undefined
			&& self.options.ableToInitSelectedItems== true)
		{

			//Now it supports uncheck some items
			var selectedItemIds = {};
			var parentSelectedItemIds = {};
			for(var i = 0 ; i < self.options.data.length;i++)
			{
				if(self.options.data[i].Selected!=undefined && 
					self.options.data[i].Selected==true)
				{
					if(parentSelectedItemIds[self.options.data[i].Id]==null)
					{
						parentSelectedItemIds[self.options.data[i].Id] = true;
					}
				}
				var children = self.options.data[i].Children;
				for(var j = 0 ; j < children.length;j++)
				{
					if(children[j].Selected!=undefined && 
					children[j].Selected==true)
					{
						if(selectedItemIds[children[j].Id]==null)
						{
							selectedItemIds[children[j].Id] = true;
						}
					}

					
				}
					
			}
			for(id in parentSelectedItemIds)
			{
				//Init all option-item-checkbox are checked
				$('#' + controlId).find('.option-item-checkbox[optionId="' + id + '"]').prop('checked', true);
				$('#' + controlId).find('.option-item-container[optionId="'+ id + '"]').addClass('option-item-selected');

			}
			for(id in selectedItemIds)
			{
				//Init all option-item-checkbox-child are checked
				$('#' + controlId).find('.option-item-checkbox-child[returnedId="'+ id +'"]').prop('checked', true);
				$('#' + controlId).find('.option-item-child[optionId="'+ id + '"]').addClass('option-item-selected');

			}
			

		}
		else
		{

			//Selected all 
			//Init all option-item-checkbox are checked
			
			$('#' + controlId).find('.option-item-checkbox').prop('checked', true);
			$('#' + controlId).find('.option-item-container').addClass('option-item-selected');
			//Init all option-item-checkbox-child are checked
			$('#' + controlId).find('.option-item-checkbox-child').prop('checked', true);
			$('#' + controlId).find('.option-item-child').addClass('option-item-selected');
			

		}
		


	}
	
			
	self.buildTree = function()
	{
		
		var innerHTML = '';
		innerHTML+= '<div class="select-container-border-1" style="float:left;">';
		innerHTML+= 	'<div style="float:left;width:35px" class="select-tree-open-close-container">';
		//innerHTML+= 		'<img src="images/Actions-arrow-left-icon-24.png" class="select-tree-open" style="display:none"/>';
		//innerHTML+= 		'<img src="images/Actions-arrow-down-icon-24.png" class="select-tree-close" />';
		innerHTML+= 		'<img src="' + options.openIconSrc +'" class="select-tree-open" style="display:none"/>';
		innerHTML+= 		'<img src="' + options.closeIconSrc + '" class="select-tree-close"  title="aaa"/>';
		
		innerHTML+= 	'</div>';
		innerHTML+= 	'<div class="option-container-inner" style="float:right; ">';
		innerHTML+= 		'<input type="text" class="form-control select-search-box" style="width:98%; float:left" placeholder="Type to search" />';
		innerHTML+= 	'</div>';
		
		
   
		innerHTML+= ' </div>';
		innerHTML+= ' <div style="clear:both"></div>';
		innerHTML+= ' <div class="option-container" style="display:none">';
		innerHTML+= 	' <div class="option-container-list select-scrollbar-2">';
		innerHTML+= 		' <ul style="float:left; width:100%" class="option-container-list-ul" >';
		
		if(options.useSampleData==undefined || options.useSampleData == false)
		{
		
			
			for(var i = 0 ; i < self.options.data.length;i++)
			{
				var li = '<li style="width:100%; float:left"  class="option-list-li" id="' + self.options.data[i].Id +'">';
					li+=    '<div class="parent-expand-collapse-container option-collapse" style="float:left;" optionId="' + self.options.data[i].Id + '">';
					li+= 		'<img src="' + options.treeCollapseIconSrc +'" class="parent-expand-icon" style="display:none"/>';
					li+= 		'<img src="' + options.treeExpandIconSrc + '" class="parent-collapse-icon"  />';
					li+=	'</div>';
					li+= '<div style="float:left; " class="option-data-container">';
					li+= 	'<div style="float:left; margin-right:2px;" class="option-item-container" optionId="' + self.options.data[i].Id + '">';
				    li+= 		'<div class="option-item">';
					li+= 			'<label class="custom-control custom-checkbox option-item-label">';
					li+= 				'<span class="custom-control-description option-item-text">' + self.options.data[i].Text + ' </span>';
					li+= 				'<input type="checkbox" class="custom-control-input option-item-checkbox" id="' + controlId +'-' + self.options.data[i].Id + '" optionId="' + self.options.data[i].Id +'">';
					li+= 				'<span class="custom-control-indicator option-item-indicator"></span>';
					li+= 			'</label>';
					li+=		'</div>';
					li+=	'</div>';
					li+=  '<!--Child Elements-->';
					
					var children = self.options.data[i].Children;
					var childHtml = '';
					
					childHtml+= '<div class="option-container-child-list" style="float:left" optionId="'+ self.options.data[i].Id +'">';
					childHtml+=	   '<ul style="float:left; width:100%" class="option-container-child-list-ul" >';
					//build child  
				
					for(var j = 0 ; j < children.length;j++)
					{
						var li1 = '';
						li1 += '<li style="width:100%" class="option-list-li-child" id="' + children[j].Id + '">';
								
						li1 +=			'<div style="float:left; width:99%; margin-right:2px;" class="option-item-child-container">';
						li1 +=				'<div class="option-item-child" optionId="' + children[j].Id + '">';
						li1 +=					'<label class="custom-control custom-checkbox option-item-label-child">';
						li1 +=						'<span class="custom-control-description option-item-text">' + children[j].Text + '</span>';
						li1 +=						'<input type="checkbox" class="custom-control-input option-item-checkbox-child" returnedId="' + children[j].Id + '" parentId="' + self.options.data[i].Id + '">';
						li1 +=						  '<span class="custom-control-indicator option-item-indicator-child"></span>';
						li1 +=				    '</label>';
						li1 +=				'</div>';
						li1 +=			'</div>';
						li1 +=			'</li>';
						childHtml += li1;

						
									
					}
					childHtml+= 	' </ul>';
					childHtml+= '</div>';
				
					li+= childHtml;
					li+= '</div>';
					li+= 	'</li>';
					
					innerHTML += li;
				
			}
		}
		innerHTML += 	'</ul>';
		innerHTML += '</div>';
		innerHTML += '<div class="option-button-container" style="margin-top:3px;border-style: solid; border-top: 0.5px solid #ddd; border-left: 0px solid #ddd;border-bottom: 0px solid #ddd;border-right: 0px solid #ddd; padding:3px">';
		innerHTML += 	'<button type="button" class="option-button option-container-expandAll btn btn-success">Expand All</button>';
		innerHTML += 	'<button type="button" class="option-button option-container-collapseAll btn btn-warning" style="margin-left:3px;">Collapse All</button>';
		innerHTML +=    '<br/>';
		innerHTML += 	'<button type="button" class="option-button option-container-selectAll btn btn-success">Select All</button>';
		innerHTML += 	'<button type="button" class="option-button option-container-clear btn btn-warning" style="margin-left:3px;">Clear All</button>';
		innerHTML += 	'<button type="button" class="option-button option-container-close btn btn-primary" style="float: right;margin-right:10px;min-width:50px;">Close</button>';
		innerHTML += '</div>';
		innerHTML += '</div>';
		innerHTML += '</div>';
		$('#'+controlId).html(innerHTML);

		
		for(var i = 0 ; i < self.options.data.length;i++)
		{
			var children = self.options.data[i].Children;
			self.parentIdTextMap[self.options.data[i].Id] = self.options.data[i].Text;


			for(var j = 0 ; j < children.length;j++)
			{
				if(self.idTextMap[children[j].Id] == null)
				{
					self.idTextMap[children[j].Id] = children[j].Text;
				} 
			}
				
		}


		self.setSelected();
		self.itemSelectedChange();
	}
	//The html must be generated before attaching event handler to each control item		
	self.buildTree();//generate html to build tree 

	/*==================== Declare event handler =============================== */
	//Tuanv2t: show the list of option
	$('#' + controlId + ' .option-container-inner').click(function (e) {
		
		if($('#' + controlId).find('.select-tree-close').is(":visible"))
		{
			self.showTree();
		
			$('#' + controlId).find('.select-tree-open').show();
			$('#' + controlId).find('.select-tree-close').hide();
		}
		

	});
	//click on containter of icon open/close
	$('#' + controlId + ' .select-tree-open-close-container').click(function (e) {
		//Tuanv2t: not allow this event to escalte to its parent
		if (event.stopPropagation) {
			event.stopPropagation();
		}
		else if (window.event) {
			window.event.cancelBubble = true;
		}
		
		if($('#' + controlId).find('.select-tree-open').is(":visible"))
		{
			self.hideTree();
			
		}
		else
		{
			self.showTree();
			$('#' + controlId).find('.select-tree-open').show();
			$('#' + controlId).find('.select-tree-close').hide();
		}

	});
	
	//Tuanv2t: hide the list of option
	$('#' + controlId + ' .option-container-close').click(function (e) {
	   self.hideTree();
	   
	});
	
	self.search = function()
	{
		
		var searchText = $('#' + controlId + ' .select-search-box').val().trim();
		if(searchText.length > 0){
			self.filteredItems = {};
			searchText = searchText.toLowerCase();
			
			for(var i = 0 ; i < options.data.length ; i ++)
			{
				var item = {};
				var childItems = [];
				
				var hasBeenMatched = false;
				if(options.data[i].Text.toLowerCase().indexOf(searchText)>=0 )
				{
					hasBeenMatched = true;
				}
				item.Id = options.data[i].Id;
				item.Text = options.data[i].Text;
				
				var children =options.data[i].Children;
				if(children!=null)
				{
					for(var j = 0; j < children.length;j++)
					{
						if(children[j].Text.toLowerCase().indexOf(searchText)>=0 )
						{
							hasBeenMatched = true;
							childItems.push({Id:children[j].Id,Text:children[j].Text});
						}
						
					}
				}
				if(hasBeenMatched)
				{
					item.Children = childItems;
					
					if(self.filteredItems[item.Id]==null){
						self.filteredItems[item.Id] = item;
					}
				}
			}
			
			
		}
		else{
			//show all, set the original data
			self.filteredItems = {};
			for(var i = 0 ; i < options.data.length ; i ++)
			{
				
				self.filteredItems[options.data[i].Id] = options.data[i];
				
			}
		}
		
		//Hide all items does not match filter of Text 
		for(var i = 0 ; i < options.data.length ; i ++)
		{
			if(self.filteredItems[options.data[i].Id]==null)
			{
				//hide not matched items
				$('#' + controlId + ' .option-list-li[id="' + options.data[i].Id+'"]').hide(); 
			}
			else
			{
				//debugger;
				var li = $('#' + controlId + ' .option-list-li[id="' + options.data[i].Id+'"]');
				li.show();
				var item = self.filteredItems[options.data[i].Id];
				li.find('.option-list-li-child').hide();
				//only show matched child li
				for(var j = 0 ; j < item.Children.length;j++)
				{
					li.find('.option-list-li-child[id="' + item.Children[j].Id + '"]').show();
				}

				//if the parent option is collapsed, automatically expand it
				if($('#' + controlId + ' .parent-expand-collapse-container[optionId="' + options.data[i].Id + '"]').hasClass('option-collapse'))
				{
					//trigger click event to expand it 
					$('#' + controlId + ' .parent-expand-collapse-container[optionId="' + options.data[i].Id + '"]').trigger('click');
				}
			}
		}
		
		
	}
	//select-search-box change
	$('#' + controlId + ' .select-search-box').on('input',function(e){
		self.search();
	});

	/* ====================== Parent level ======================================== */
	//Tuanv2t: event handler for option-item-container
	$('#' + controlId + ' .option-item-checkbox-container').click(function (e) {

	  //check if the checkbox is checked or not
	  var item = $(e.target);
	  if(item.hasClass('option-item-checkbox-container')){
	   
	   //when user click on div outer the checkbox and span , trigger click for input 
		var checkbox = item.find('.option-item-checkbox');
		checkbox.trigger('click');
	  }
	  
	});
	//Tuanv2t: event handler for option-item-checkbox
	 $('#' + controlId + ' .option-item-checkbox').change(function() {
	  var optionItemContainer = $(this).parent().parent().parent();//find div option-item-container
		var optionListLi = optionItemContainer.parent();//find option-list-li
		
	
		if($(this).is(":checked")) {
			optionItemContainer.addClass('option-item-selected');
			optionItemContainer.removeClass('option-item-unselected');
			$(this).find('.option-item-child').removeClass("item-disabled");
			
			//Mark not disable all its children
			optionListLi.find('.option-item-child').removeClass("item-disabled");
			//Enable all option-item-checkbox-child
			//optionListLi.find('.option-item-checkbox-child').attr('disabled', false);
			//Check all option-item-checkbox-child
			optionListLi.find('.option-item-checkbox-child').prop('checked', true);
			optionListLi.find('.option-item-child').addClass("option-item-selected");
			//Call the callback
			self.itemSelectedChange();
		}
		else{
			optionItemContainer.addClass('option-item-unselected');
			optionItemContainer.removeClass('option-item-selected');
			
			//Mark disable all its children
			//debugger;
			
			optionListLi.find('.option-item-child').addClass("item-disabled");
			optionListLi.find('.option-item-child').removeClass("option-item-selected");
			//uncheck all option-item-checkbox-child
			
			optionListLi.find('.option-item-checkbox-child').prop('checked', false); 
			//Disable all option-item-checkbox-child
			//optionListLi.find('.option-item-checkbox-child').attr('disabled', true);

			//Call the callback
			self.itemSelectedChange();
		}

		
	});
	 $('#' + controlId + ' .option-container-inner-item-icon').click(function(event) {
		//debugger;
		//Tuanv2t: not allow this event to escalte to its parent
		if (event.stopPropagation) {
			event.stopPropagation();
		}
		else if (window.event) {
			window.event.cancelBubble = true;
		}
		//remove this item
		
		$(this).parent().remove();
		//show it back to the list 
		
		
	});
	
	//Click on icon expand/collapse
	 $('#' + controlId + ' .parent-expand-collapse-container').click(function(event) {
		var container = $(this);
		//debugger;
		if(!container.hasClass('parent-expand-collapse-container'))
			{
				return;
			}
		
		if(container.hasClass('option-collapse')){
			//Being collapsed -> click to  expand
			container.find('.parent-collapse-icon').hide();
			container.find('.parent-expand-icon').show();
			//show children
			var optionId = container.attr('optionId');
			
			$('#' + controlId + ' .option-container-child-list[optionId="' + optionId +'"]').slideDown('slow');
			container.removeClass('option-collapse');
			container.addClass('option-expand');

		}

		else if(container.hasClass('option-expand')){
			//Being expanded -> click to collapse
			container.find('.parent-collapse-icon').show();
			container.find('.parent-expand-icon').hide();
			var optionId = container.attr('optionId');
			
			$('#' + controlId + ' .option-container-child-list[optionId="' + optionId +'"]').slideUp('slow');
			container.removeClass('option-expand');
			container.addClass('option-collapse');

		}
		
		
	});
	
	/* ====================== Child level ======================================== */
	
	$('#' + controlId + ' .option-item-child').click(function (e) {
		
	  //check if the checkbox is checked or not
	  var item = $(e.target);
	  if(item.hasClass('option-item-child')){
	   if(item.hasClass('item-disabled')){
		return;//it's being disabled because its parent is unchecked
	   }
	   //when user click on div outer the checkbox and span , trigger click for input 
		var checkbox = item.find('.option-item-checkbox-child');
		checkbox.trigger('click');
	  }
	  
	});
	
	//Handel click event on each checkbox
	 $('#' + controlId + ' .option-item-checkbox-child').change(function() {
	  var optionItemChild = $(this).parent().parent();
	
		
		
		if($(this).is(":checked")) {
			
			optionItemChild.addClass('option-item-selected');
			optionItemChild.removeClass('option-item-unselected');
		}
		else{
			optionItemChild.addClass('option-item-unselected');
			optionItemChild.removeClass('option-item-selected');
		}
		
		//logic to set auto check for parent
	
		var parentId = $(this).attr('parentId');
		var countChildrent = $('#' + controlId + ' .option-item-checkbox-child[parentId="' + parentId + '"]').length;
		var countChildrentChecked = $('#' + controlId + ' .option-item-checkbox-child[parentId="' + parentId + '"]:checked').length;
		
	
		if(countChildrentChecked==0){
			//set unchecked for the parent
			$('#' + controlId).find('.option-item-checkbox[optionId="' + parentId + '"]').prop('checked', false);
			$('#' + controlId).find('.option-item-container[optionId="' + parentId + '"]').removeClass('option-item-selected');
			$('#' + controlId).find('.option-item-container[optionId="' + parentId + '"]').addClass('option-item-unselected');

		}
		else{
			//if(countChildrent==countChildrentChecked)
			{
				//set checked for the parent
				if(!$('#' + controlId).find('.option-item-checkbox[optionId="' + parentId + '"]').is(':checked'))
				{
					$('#' + controlId).find('.option-item-checkbox[optionId="' + parentId + '"]').prop('checked', true);
					$('#' + controlId).find('.option-item-container[optionId="' + parentId + '"]').addClass('option-item-selected');
					$('#' + controlId).find('.option-item-container[optionId="' + parentId + '"]').removeClass('option-item-unselected');
				}
			}
			
		}
		self.itemSelectedChange();
		
	});
	
	 
	
	//Clear all
	$('#' + controlId + ' .option-container-clear').click(function (e) {
		
	  //clear all
	  $('#' + controlId).find('.option-item-checkbox').prop('checked', false);
	  $('#' + controlId).find('.option-item-checkbox').prop('checked', false);
	  
	  //child 
	  $('#' + controlId).find('.option-item-checkbox-child').prop('checked', false);
	  $('#' + controlId).find('.option-item-selected').removeClass('option-item-selected');
	  $('#' + controlId).find('.option-item-container').addClass('option-item-unselected');
	  $('#' + controlId).find('.option-item-child').addClass('option-item-unselected');

	  //disable all child 
	  //$('#' + controlId).find('.option-item-checkbox-child').attr('disabled', true);
	  self.itemSelectedChange();
	});
	//Select All
	$('#' + controlId + ' .option-container-selectAll').click(function (e) {
		
	  
	  $('#' + controlId).find('.option-item-checkbox').prop('checked', true);
	  $('#' + controlId).find('.option-item-checkbox').prop('checked', true);
	  
	 
	  $('#' + controlId).find('.option-item-checkbox-child').prop('checked', true);
	  $('#' + controlId).find('.option-item-container').addClass('option-item-selected');
	  $('#' + controlId).find('.option-item-child').addClass('option-item-selected');
	  //enable all child 
	  //$('#' + controlId).find('.option-item-checkbox-child').attr('disabled', false);
	  self.itemSelectedChange();
	});
	//Expand All
	$('#' + controlId + ' .option-container-expandAll').click(function (e) {
		
	  $('#' + controlId  + ' .option-container-child-list').slideDown('slow');
	  $('#' + controlId  + ' .parent-expand-collapse-container').removeClass('option-collapse');
	  $('#' + controlId  + ' .parent-expand-collapse-container').addClass('option-expand');

	  $('#' + controlId  + ' .parent-collapse-icon').hide();
	  $('#' + controlId  + ' .parent-expand-icon').show();
	});
	//Collapse All
	$('#' + controlId + ' .option-container-collapseAll').click(function (e) {
		
	  $('#' + controlId  + ' .option-container-child-list').slideUp('slow');
	  $('#' + controlId  + ' .parent-expand-collapse-container').addClass('option-collapse');
	  $('#' + controlId  + ' .parent-expand-collapse-container').removeClass('option-expand');
	 
	  $('#' + controlId  + ' .parent-collapse-icon').show();
	  $('#' + controlId  + ' .parent-expand-icon').hide();

	

	});

	$('body').click(function (e) 
	{
            var item = $(e.target);
			
			if(!item.hasClass('select-container-border-1')
			&&!item.hasClass('option-container-inner')
			&&!item.hasClass('select-search-box')
			&&!item.hasClass('select-tree-open-close-container')
			&&!item.hasClass('select-tree-open')
			&&!item.hasClass('select-tree-close')
			&&!item.hasClass('option-container')
			&&!item.hasClass('option-container-list')
			&&!item.hasClass('option-container-list-ul')
			&&!item.hasClass('option-list-li')
			&&!item.hasClass('option-item-container')
			&&!item.hasClass('option-item')
			&&!item.hasClass('option-item-label')
			&&!item.hasClass('option-item-text')
			&&!item.hasClass('option-item-checkbox')
			&&!item.hasClass('option-item-indicator')
			&&!item.hasClass('option-container-selectAll')
			&&!item.hasClass('option-container-clear')
			&&!item.hasClass('option-container-close')
			&&!item.hasClass('option-container-child-list')
			&&!item.hasClass('option-container-child-list-ul')
			&&!item.hasClass('option-list-li-child')
			&&!item.hasClass('option-item-child-container')
			&&!item.hasClass('option-item-child')
			&&!item.hasClass('option-item-label-child')
			&&!item.hasClass('option-item-checkbox-child')
			&&!item.hasClass('option-item-indicator-child')
			&&!item.hasClass('option-button')
			&&!item.hasClass('option-button-container')
			&&!item.hasClass('parent-expand-collapse-container')
			&&!item.hasClass('parent-expand-icon')
			&&!item.hasClass('parent-collapse-icon')
		
			)
			{
				self.hideTree();
			
			}
	});

	/* Default action right after generating HTML */
	//Hide all child level items
	//$('.option-container-child-list').hide();
	$('.option-container-child-list').slideUp('slow');



	return self;//return self for outside reference 
};