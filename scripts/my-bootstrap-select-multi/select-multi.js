/*
Author: tuanv2t@gmail.com
*/
$.fn.multiSelect = function(options) {
    var self = $(this);
	 self.options = options;
	 self.filteredItems = {};
	 self.idTextMap = {};
	var controlId = self.attr('id');
	
	
	
	self.selectedItems = {};
	
	self.itemSelectedChange= function(){
		//call callback function 
		if(options.itemSelectedChange!=undefined){
			var selectedId=[];
			self.selectedItems = {};//clear selected items
			$('#' + controlId + ' .option-item-checkbox').each(function( key, value ) {
			  var chk = $(value);
			  
			  if(chk.is(":checked"))
			  {
				var returnedId = chk.attr('returnedId');
				selectedId.push({Id:returnedId,Text:self.idTextMap[returnedId]});
				if(self.selectedItems[returnedId]==null){
					//rememeber selected item 
					self.selectedItems[returnedId] = self.idTextMap[returnedId];
				}
			  }
			  
			});
			options.itemSelectedChange(selectedId);
			
		}
	}
	self.showSelect = function(){
		$( '#' + controlId + ' .option-container' ).slideDown( "slow", function() {
			// Animation complete.
		});
		var indexHighest = 0;
        $("*").each(function () {
            var indexCurrent = parseInt($(this).css("zIndex"), 10);
            if (indexCurrent > indexHighest) {
                indexHighest = indexCurrent;
            }
        });
        $('#' + controlId).css('zIndex', indexHighest + 1);
	}
	self.hideSelect = function(){
		if($( '#' + controlId + ' .option-container' ).is(":visible")){
			$('#' + controlId).find('.select-tree-open').hide();
	 	    $('#' + controlId).find('.select-tree-close').show();

			$( '#' + controlId + ' .option-container' ).slideUp( "slow", function() {
				// Animation complete.
			});
		}
	}
	
	self.buildSelect = function()
	{
		for(var i = 0 ; i < self.options.data.length;i++)
		{
			if(self.idTextMap[self.options.data[i].Id] == null){
				self.idTextMap[self.options.data[i].Id] = self.options.data[i].Text;
			} 	
		}
		var innerHTML = '';
		innerHTML+= '<div class="select-container-border-1" style="float:left;">';
		innerHTML+= 	'<div style="float:left;width:35px" class="select-tree-open-close-container">';
		//innerHTML+= 		'<img src="images/Actions-arrow-left-icon-24.png" class="select-tree-open" style="display:none"/>';
		//innerHTML+= 		'<img src="images/Actions-arrow-down-icon-24.png" class="select-tree-close" />';
		innerHTML+= 		'<img src="' + options.openIconSrc +'" class="select-tree-open" style="display:none"/>';
		innerHTML+= 		'<img src="' + options.closeIconSrc + '" class="select-tree-close" />';
		
		innerHTML+= 	'</div>';
		innerHTML+= 	'<div class="option-container-inner" style="float:right; ">';
		innerHTML+= 		'<input type="text" class="form-control select-search-box" style="width:98%; float:left" placeholder="Type to search" />';
		innerHTML+= 	'</div>';
		
		
   
		innerHTML+= ' </div>';
		innerHTML+= ' <div style="clear:both"></div>';
		innerHTML+= ' <div class="option-container" style="display:none">';
		innerHTML+= 	' <div class="option-container-list select-scrollbar-2">';
		innerHTML+= 		' <ul style="float:left; width:100%" class="option-container-list-ul" >';

		var ulInnerHTML = '';
		if(options.useSampleData==undefined || options.useSampleData == false)
		{
		
			
			for(var i = 0 ; i < self.options.data.length;i++)
			{
				//build child first 
				
				var li = '<li style="width:100%"  class="option-list-li" id="' + self.options.data[i].Id + '">';
					li+= 	'<div style="float:left; margin-right:2px;" class="option-item-container">';
					li+= 		'<div class="option-item">';
					li+= 			'<label class="custom-control custom-checkbox option-item-label">';
					li+= 				'<span class="custom-control-description option-item-text">' + self.options.data[i].Text + ' </span>';
					li+= 				'<input type="checkbox" class="custom-control-input option-item-checkbox" id="' + controlId +'-' + self.options.data[i].Id + '" returnedId="' + self.options.data[i].Id + '">';
					li+= 				'<span class="custom-control-indicator option-item-indicator"></span>';
					li+= 			'</label>';
					li+= 		'</div>';
					li+=	'</div>';
					
					li+= '</li>';
					//ul.append(li);
					ulInnerHTML += li;
				
			}
		}
		innerHTML += ulInnerHTML;
		innerHTML += 	'</ul>';
		innerHTML += '</div>';
		innerHTML += '<div class="option-button" style="margin-top:3px;border-style: solid; border-top: 0.5px solid #ddd;border-left: 0px solid #ddd;border-bottom: 0px solid #ddd;border-right: 0px solid #ddd; padding:3px">';
		innerHTML += 	'<button type="button" class="option-container-selectAll btn btn-success">Select All</button>';
		innerHTML += 	'<button type="button" class="option-container-clear btn btn-warning" style="margin-left:3px;">Clear All</button>';
		innerHTML += 	'<button type="button" class="option-container-close btn btn-primary" style="float: right;margin-right:10px">Close</button>';
		innerHTML += '</div>';
		innerHTML += '</div>';
		innerHTML += '</div>';
		$('#'+controlId).html(innerHTML);
		//Init all option-item-checkbox are checked
		$('#' + controlId).find('.option-item-checkbox').prop('checked', true);
		$('#' + controlId).find('.option-item-container').addClass('option-item-selected');
		
		
		
		self.itemSelectedChange();
		
	}
	
			
	
	
	//generate html of the select here 
	//The html must be generated before attaching event handler to each control item
	self.buildSelect();//generate html to build treen 
	
	//Tuanv2t: event handler for option-item-checkbox
	
	 $('#' + controlId + ' .option-item-checkbox').change(function() {
	 
	  var optionItemContainer = $(this).parent().parent().parent();//find div option-item-container
		var optionListLi = optionItemContainer.parent();//find option-list-li
		
		
		if($(this).is(":checked")) {
			optionItemContainer.addClass('option-item-selected');
			optionItemContainer.removeClass('option-item-unselected');
			
		}
		else{
			optionItemContainer.addClass('option-item-unselected');
			optionItemContainer.removeClass('option-item-selected');
			
			
		}
		self.itemSelectedChange();
		
	});
	
	//Tuanv2t: show the list of option
	$('#' + controlId + ' .option-container-inner').click(function (e) {
		
		if($('#' + controlId).find('.select-tree-close').is(":visible"))
		{
			self.showSelect();
		
			$('#' + controlId).find('.select-tree-open').show();
			$('#' + controlId).find('.select-tree-close').hide();
		}
		

	});
	//click on containter of icon open/close
	$('#' + controlId + ' .select-tree-open-close-container').click(function (e) {
		//Tuanv2t: not allow this event to escalte to its parent
		if (event.stopPropagation) {
			event.stopPropagation();
		}
		else if (window.event) {
			window.event.cancelBubble = true;
		}
		
		if($('#' + controlId).find('.select-tree-open').is(":visible"))
		{
			self.hideSelect();
		
		}
		else
		{
			self.showSelect();
			$('#' + controlId).find('.select-tree-open').show();
			$('#' + controlId).find('.select-tree-close').hide();
		}

	});
	
	//Tuanv2t: hide the list of option
	$('#' + controlId + ' .option-container-close').click(function (e) {
	  self.hideSelect();
	});
	
	self.search = function()
	{
		
		var searchText = $('#' + controlId + ' .select-search-box').val().trim();
		if(searchText.length > 0){
			self.filteredItems = {};
			searchText = searchText.toLowerCase();
			
			for(var i = 0 ; i < options.data.length ; i ++)
			{
				var item = {};
				if(options.data[i].Text.toLowerCase().indexOf(searchText)>=0 )
				{
					item.Id = options.data[i].Id;
					item.Text = options.data[i].Text;
					
					if(self.filteredItems[item.Id]==null){
						self.filteredItems[item.Id] = item;
					}
					
				}
				
			}
			
			
		}
		else{
			//show all, set the original data
			self.filteredItems = {};
			for(var i = 0 ; i < options.data.length ; i ++)
			{
				if(self.filteredItems[options.data[i].Id]==null)
				{
					self.filteredItems[options.data[i].Id] = options.data[i];
				}
			}
			
			
		}
		
		
		//Hide all items does not match filter of Text 
		for(var i = 0 ; i < options.data.length ; i ++)
		{
			if(self.filteredItems[options.data[i].Id]==null)
			{
				//hide
				$('#' + controlId + ' .option-list-li[id="' + options.data[i].Id+'"]').hide(); 
			}
			else
			{
				$('#' + controlId + ' .option-list-li[id="' + options.data[i].Id+'"]').show(); 
			}
		}
		
	}
	//select-search-box change
	$('#' + controlId + ' .select-search-box').on('input',function(e){
		self.search();
	});

	//Tuanv2t: event handler for option-item-container
	$('#' + controlId + ' .option-item-checkbox-container').click(function (e) {

	  //check if the checkbox is checked or not
	  var item = $(e.target);
	  if(item.hasClass('option-item-checkbox-container')){
	   
	   //when user click on div outer the checkbox and span , trigger click for input 
		var checkbox = item.find('.option-item-checkbox');
		checkbox.trigger('click');
	  }
	  
	});
	
	
	 $('#' + controlId + ' .option-container-inner-item-icon').click(function(event) {
		//debugger;
		//Tuanv2t: not allow this event to escalte to its parent
		if (event.stopPropagation) {
			event.stopPropagation();
		}
		else if (window.event) {
			window.event.cancelBubble = true;
		}
		//remove this item
		
		$(this).parent().remove();
		//show it back to the list 
		
		
	});
	
	 $('#' + controlId + ' .option-item').click(function (e) {

        //user has selected option-item-container-single
        var item = $(e.target);
        if (item.hasClass('option-item')) {
            //when user click on div outer the checkbox and span , trigger click for input 
            var checkbox = item.find('.option-item-checkbox');
            checkbox.trigger('click');
        }

    });
	
	/*Child*/
	
	
	//Clear all
	$('#' + controlId + ' .option-container-clear').click(function (e) {
		
	  //clear all
	  $('#' + controlId).find('.option-item-checkbox').prop('checked', false);
	  $('#' + controlId).find('.option-item-checkbox').prop('checked', false);
	  
	  //child 
	  $('#' + controlId).find('.option-item-checkbox-child').prop('checked', false);
	  $('#' + controlId).find('.option-item-selected').removeClass('option-item-selected');
	  
	  self.itemSelectedChange();
	});
	//Select All
	$('#' + controlId + ' .option-container-selectAll').click(function (e) {
	  $('#' + controlId).find('.option-item-checkbox').prop('checked', true);
	  $('#' + controlId).find('.option-item-checkbox').prop('checked', true);
	  
	  $('#' + controlId).find('.option-item-container').addClass('option-item-selected');
	  
	  self.itemSelectedChange();
	  
	});
	//auto hide control
	$('body').click(function (e) 
	{
            var item = $(e.target);
			
			if(!item.hasClass('select-container-border-1')
			&&!item.hasClass('option-container-inner')
			&&!item.hasClass('select-search-box')
			&&!item.hasClass('select-tree-open-close-container')
			&&!item.hasClass('select-tree-open')
			&&!item.hasClass('select-tree-close')
			&&!item.hasClass('option-container')
			&&!item.hasClass('option-container-list')
			&&!item.hasClass('option-container-list-ul')
			&&!item.hasClass('option-list-li')
			&&!item.hasClass('option-item-container')
			&&!item.hasClass('option-item')
			&&!item.hasClass('option-item-label')
			&&!item.hasClass('option-item-text')
			&&!item.hasClass('option-item-checkbox')
			&&!item.hasClass('option-item-indicator')
			&&!item.hasClass('option-container-selectAll')
			&&!item.hasClass('option-container-clear')
			&&!item.hasClass('option-container-close')
			&&!item.hasClass('option-container-child-list')
			&&!item.hasClass('option-container-child-list-ul')
			&&!item.hasClass('option-list-li-child')
			&&!item.hasClass('option-item-child-container')
			&&!item.hasClass('option-item-child')
			&&!item.hasClass('option-item-label-child')
			&&!item.hasClass('option-item-checkbox-child')
			&&!item.hasClass('option-item-indicator-child')
			&&!item.hasClass('option-button')
		
			)
			{
				self.hideSelect();
			
			}
	});

	return self;
};